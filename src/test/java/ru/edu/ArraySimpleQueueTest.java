package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleQueueTest {
    ArraySimpleQueue<String> testQueue = new ArraySimpleQueue<>(5);
    ArraySimpleQueue<String> testEmptyQueue = new ArraySimpleQueue<>();

    @Before
    public void setUp() {
        testQueue.offer("first");
        testQueue.offer("second");
        testQueue.offer("third");
        testQueue.offer("fourth");
    }

    @Test
    public void offer() {
        assertEquals(true, testQueue.offer("fifth"));

        assertEquals(false, testQueue.offer("sixth"));
    }

    @Test (expected = NullPointerException.class)
    public void offerException() {
        testQueue.offer(null);
    }

    @Test
    public void poll() {
        assertEquals("first", testQueue.poll());
        assertEquals("second", testQueue.poll());
        assertEquals("third", testQueue.poll());
        assertEquals("fourth", testQueue.poll());
        assertEquals(null, testQueue.poll());

        assertEquals(null, testEmptyQueue.poll());
        testEmptyQueue.offer("first");
        assertEquals("first", testEmptyQueue.poll());
        assertEquals(null, testEmptyQueue.poll());
    }

    @Test
    public void peek() {
        assertEquals("first", testQueue.peek());

        assertEquals(null, testEmptyQueue.peek());
    }

    @Test
    public void size() {
        assertEquals(4, testQueue.size());
        testQueue.poll();
        assertEquals(3, testQueue.size());

        assertEquals(0, testEmptyQueue.size());
        testEmptyQueue.offer("first");
        assertEquals(1, testEmptyQueue.size());
    }

    @Test
    public void capacity() {
        assertEquals(10, testEmptyQueue.capacity());
        assertEquals(1, testQueue.capacity());
    }
}