package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedSimpleListTest {
    LinkedSimpleList<Integer> testList = new LinkedSimpleList<>();

    @Before
    public void setUp() {
        testList.add(1);
        testList.add(2);
        testList.add(3);
        testList.add(4);
        testList.add(1);
        testList.add(5);
    }

    @Test
    public void add() {
        testList.add(1);

        assertEquals(new Integer(1), testList.get(0));
        assertEquals(new Integer(2), testList.get(1));
        assertEquals(new Integer(3), testList.get(2));
        assertEquals(new Integer(4), testList.get(3));
        assertEquals(new Integer(1), testList.get(4));
        assertEquals(new Integer(5), testList.get(5));
        assertEquals(new Integer(1), testList.get(6));
    }

    @Test
    public void set() {
        testList.set(4, 5);

        assertEquals(new Integer(1), testList.get(0));
        assertEquals(new Integer(2), testList.get(1));
        assertEquals(new Integer(3), testList.get(2));
        assertEquals(new Integer(4), testList.get(3));
        assertEquals(new Integer(5), testList.get(4));
        assertEquals(new Integer(5), testList.get(5));
    }

    @Test
    public void get() {
        assertEquals(new Integer(1), testList.get(0));
        assertEquals(new Integer(2), testList.get(1));
        assertEquals(new Integer(3), testList.get(2));
        assertEquals(new Integer(4), testList.get(3));
        assertEquals(new Integer(1), testList.get(4));
        assertEquals(new Integer(5), testList.get(5));
    }

    @Test
    public void remove() {
        testList.remove(4);

        assertEquals(new Integer(1), testList.get(0));
        assertEquals(new Integer(2), testList.get(1));
        assertEquals(new Integer(3), testList.get(2));
        assertEquals(new Integer(4), testList.get(3));
        assertEquals(new Integer(5), testList.get(4));
    }

    @Test
    public void indexOf() {
        assertEquals(0,testList.indexOf(1));

        assertEquals(-1,testList.indexOf(10));
    }

    @Test
    public void size() {
        assertEquals(6, testList.size());
    }

    @Test (expected = ArrayIndexOutOfBoundsException.class)
    public void findNode() {
        testList.findNode(8);
    }
}