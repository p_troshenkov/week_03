package ru.edu;


public class ArraySimpleQueue<T> implements SimpleQueue<T> {
    /**
     * Количество объектов в списке.
     */
    private int size;
    /**
     * Массив в качестве основы списка.
     */
    private T[] array;
    /**
     * Вместимость списка по умолчанию.
     */
    private static final int DEFAULT_CAPACITY = 10;
    /**
     * Конструктор с длиной массива по умолчанию.
     */
    public ArraySimpleQueue() {
        array = (T[]) new Object[DEFAULT_CAPACITY];
    }
    /**
     * Конструктор с заданной длиной массива.
     * @param capacity
     */
    public ArraySimpleQueue(final int capacity) {
        array = (T[]) new Object[capacity];
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (value == null) {
            throw new NullPointerException("cannot add null value in queue");
        }

        if (size < array.length) {
            array[size] = value;
            size++;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        if (size == 0) {
            return null;
        }

        T[] tempArray = (T[]) new Object[array.length];
        T tempElement = array[0];

        System.arraycopy(array, 1, tempArray, 0, array.length - 1);
        array = tempArray;
        size--;
        return tempElement;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        if (size == 0) {
            return null;
        }
        return array[0];
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return array.length - size;
    }

}
