package ru.edu;


public class ArraySimpleList<T> implements SimpleList<T> {
    /**
     * Вместимость списка.
     */
    private int capacity;
    /**
     * Количество объектов в списке.
     */
    private int size;
    /**
     * Массив в качестве основы списка.
     */
    private T[] array;
    /**
     * Вместимость списка по умолчанию.
     */
    private static final int DEFAULT_CAPACITY = 10;
    /**
     * Конструктор списка по умолчанию, с вместимостью 10 объектов.
     */
    public ArraySimpleList() {
        array = (T[]) new Object[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
    }

    /**
     * Конструктор списка с заданной вместимостью.
     * @param initCapacity
     */
    public ArraySimpleList(final int initCapacity) {
        capacity = initCapacity;
        array = (T[]) new Object[initCapacity];
    }
    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        if ((size + 1) > capacity) {
            capacity *= 2;
            T[] temp = (T[]) new Object[capacity];
            System.arraycopy(array, 0, temp, 0, array.length);
            array = temp;
        }
        array[size] = value;
        size++;
    }
    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        if ((index >= size) || (index < 0)) {
            throw new ArrayIndexOutOfBoundsException("Неверный индекс");
        }

        array[index] = value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        if ((index >= size) || (index < 0)) {
            throw new ArrayIndexOutOfBoundsException("Неверный индекс");
        }

        return array[index];
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        if ((index >= size) || (index < 0)) {
            throw new ArrayIndexOutOfBoundsException("Неверный индекс");
        }
        System.arraycopy(array, index + 1, array, index, size - index - 1);
        size--;
        array[size] = null;
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     */
    @Override
    public int indexOf(final T value) {
        if (value == null) {
            for (int i = 0; i < size; i++) {
                if (array[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (value.equals(array[i])) {
                    return i;
                }
            }
        }

        return -1;
    }
    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
}
