package ru.edu;



public class LinkedSimpleList<T> implements SimpleList<T> {
    /**
     * Узел начала списка.
     */
    private Node<T> first = new Node<>();
    /**
     * Узел конца списка.
     */
    private Node<T> last = new Node<>();
    /**
     * Количество элементов в списке.
     */
    private int size;
    /**
     * Класс описания узлов.
     * @param <T>
     */
    private class Node<T> {
        /**
         * Ссылка на предыдущий элемент.
         */
        private Node prev;
        /**
         * Значение элемента.
         */
        private T value;
        /**
         * Ссылка на следующий элемент.
         */
        private Node next;
    }
    /**
     * При создании списка создаются начальный и конечный узел,
     * ссылающиеся друг на друга.
     */
    LinkedSimpleList() {
        first.next = last;
        last.prev = first;
    }
    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        Node<T> node = new Node<>();
        node.value = value;

        Node<T> lastNode = last.prev;
        lastNode.next = node;
        node.prev = lastNode;
        last.prev = node;
        node.next = last;

        size++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        findNode(index).value = value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        return findNode(index).value;
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        Node<T> removeNode = findNode(index);
        removeNode.prev.next = removeNode.next;
        removeNode.next.prev = removeNode.prev;
        size--;
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     */
    @Override
    public int indexOf(final T value) {
        Node<T> find = first.next;
        for (int i = 0; i < size; i++) {
            if (find.value.equals(value)) {
                return i;
            }
            find = find.next;
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
    /**
     * Поиск элемента по порядковому номеру.
     *
     * @param index номер
     * @return элемент
     */
    public Node<T> findNode(final int index) {
        if ((index < 0) || (index >= size)) {
            throw new ArrayIndexOutOfBoundsException("Неверный индекс");
        }

        Node<T> find = null;

        if (index < size / 2) {
            find = first.next;
            for (int i = 0; i < index; i++) {
                find = find.next;
            }
        } else {
            find = last.prev;
            for (int i = size - 1; i > index; i--) {
                find = find.prev;
            }
        }
        return find;
    }


}
