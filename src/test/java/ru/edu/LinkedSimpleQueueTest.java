package ru.edu;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedSimpleQueueTest {
    LinkedSimpleQueue<String> testLinkedQueue = new LinkedSimpleQueue<>();

    @Test
    public void offer() {
        assertEquals(true, testLinkedQueue.offer("first"));
    }

    @Test (expected = NullPointerException.class)
    public void offerException() {
        testLinkedQueue.offer(null);
    }

    @Test
    public void poll() {
        testLinkedQueue.offer("first");
        testLinkedQueue.offer("second");
        testLinkedQueue.offer("third");

        assertEquals("first", testLinkedQueue.poll());
        assertEquals("second", testLinkedQueue.poll());
        assertEquals("third", testLinkedQueue.poll());
    }

    @Test
    public void peek() {
        testLinkedQueue.offer("first");
        testLinkedQueue.offer("second");
        testLinkedQueue.offer("third");

        assertEquals("first", testLinkedQueue.peek());
        assertEquals("first", testLinkedQueue.peek());
        assertEquals("first", testLinkedQueue.peek());
    }

    @Test
    public void size() {
        assertEquals(0, testLinkedQueue.size());

        testLinkedQueue.offer("first");
        testLinkedQueue.offer("second");

        assertEquals(2, testLinkedQueue.size());

        testLinkedQueue.peek();

        assertEquals(2, testLinkedQueue.size());

        testLinkedQueue.poll();

        assertEquals(1, testLinkedQueue.size());
    }

    @Test
    public void capacity() {
        assertEquals(-1, testLinkedQueue.capacity());
    }
}