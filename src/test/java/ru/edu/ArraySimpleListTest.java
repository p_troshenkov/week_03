package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleListTest {
    ArraySimpleList<String> testStringList = new ArraySimpleList<>();
    ArraySimpleList<String> testGrowList = new ArraySimpleList<>(1);

    @Before
    public void setUp() {
        testStringList.add("first");
        testStringList.add("second");
        testStringList.add("third");
        testStringList.add("first");

        testGrowList.add("1");
    }

    @Test
    public void add() {
        testStringList.add("fourth");

        assertEquals("first", testStringList.get(0));
        assertEquals("second", testStringList.get(1));
        assertEquals("third", testStringList.get(2));
        assertEquals("first", testStringList.get(3));
        assertEquals("fourth", testStringList.get(4));

        testGrowList.add("2");

        assertEquals("1", testGrowList.get(0));
        assertEquals("2", testGrowList.get(1));
    }

    @Test
    public void set() {
        testStringList.set(1, "first");

        assertEquals("first", testStringList.get(0));
        assertEquals("first", testStringList.get(1));
        assertEquals("third", testStringList.get(2));
        assertEquals("first", testStringList.get(3));
    }

    @Test (expected = ArrayIndexOutOfBoundsException.class)
    public void setException() {
        testStringList.set(5, "first");
    }

    @Test
    public void get() {
        assertEquals("first", testStringList.get(0));
        assertEquals("second", testStringList.get(1));
        assertEquals("third", testStringList.get(2));
        assertEquals("first", testStringList.get(3));
    }

    @Test (expected = ArrayIndexOutOfBoundsException.class)
    public void getException() {
        testStringList.get(5);
    }

    @Test
    public void remove() {
        testStringList.remove(1);

        assertEquals("first", testStringList.get(0));
        assertEquals("third", testStringList.get(1));
        assertEquals("first", testStringList.get(2));

        testStringList.remove(2);

        assertEquals("first", testStringList.get(0));
        assertEquals("third", testStringList.get(1));
    }

    @Test (expected = ArrayIndexOutOfBoundsException.class)
    public void removeException() {
        testStringList.remove(4);
    }

    @Test
    public void indexOf() {
        assertEquals(0, testStringList.indexOf("first"));
        assertEquals(2, testStringList.indexOf("third"));
        assertEquals(-1, testStringList.indexOf("word"));

        testStringList.set(1, null);
        assertEquals(1, testStringList.indexOf(null));
    }

    @Test
    public void size() {
        assertEquals(4, testStringList.size());
        assertEquals(1, testGrowList.size());
    }
}